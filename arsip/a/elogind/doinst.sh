
config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}

config etc/elogind/logind.conf.new
config etc/rc.d/rc.elogind.new
config etc/openrc/conf.d/elogind.new
config etc/openrc/init.d/elogind.new
chmod +x etc/openrc/init.d/elogind.new

# Replace systemd with elogind in /etc/pam.d/*
if [ -d /etc/pam.d ]; then
  ( cd /etc/pam.d;
    for x in *; do
      if grep -q 'pam_systemd.so' $x ; then
        echo "*** WARNING ***
        ** REPLACING pam_systemd.so with pam_elogind.so in /etc/pam.d/$x **"
        if [ -L $x ]; then
          sed 's/pam_systemd.so/pam_elogind.so/g' -i $(readlink $x)
        else
          sed 's/pam_systemd.so/pam_elogind.so/g' -i $x
        fi
      fi
    done
  )
fi

if [ -d /etc/pam.d ]; then
  if ! grep -q 'pam_elogind.so' /etc/pam.d/login ; then
    echo "** WARNING: ADDING pam_elogind.so to /etc/pam.d/login **"
    echo "# Use elogind to track user on console session" >>/etc/pam.d/login
    echo "session optional pam_elogind.so" >>/etc/pam.d/login
  fi
  # Cleaning up elogind from /etc/pam.d/system-auth if any
  if grep -q 'elogind' /etc/pam.d/system-auth ; then
    echo "** WARNING: REMOVING pam_elogind.so from /etc/pam.d/system-auth **"
    cat /etc/pam.d/system-auth | sed '/elogind/d' >/etc/pam.d/system-auth.edit
    mv /etc/pam.d/system-auth.edit /etc/pam.d/system-auth
  fi
fi

# Reload elogind
echo "Reloading elogind..."
pkill -HUP -F /run/elogind.pid
