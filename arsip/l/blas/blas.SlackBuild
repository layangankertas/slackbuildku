#!/bin/sh

# Slackware build script for BLAS

# Copyright 2014-2020 Kyle Guinn <elyk03@gmail.com>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# Modified 2020 by Ali <idnux09@gmail.com>

PRGNAM=blas
SRCNAM=lapack
VERSION=$(cat $PRGNAM.info | grep "VERSION" | cut -d = -f 2 | sed 's/"//g')
BUILD=${BUILD:-1}
TAG=${TAG:-_idn}
NUMJOBS=${NUMJOBS:-" -j$(expr $(nproc) + 1) "}

# Automatically determine the architecture we're building on:
MARCH=$( uname -m )
if [ -z "$ARCH" ]; then
  case "$MARCH" in
    i?86)    export ARCH=i586 ;;
    armv7hl) export ARCH=$MARCH ;;
    arm*)    export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
    *)       export ARCH=$MARCH ;;
  esac
fi

CWD=$(pwd)
WRK=${WRK:-/tmp/idn/$PRGNAM}
PKG=$WRK/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

unset DOWNLOAD
eval `cat $PRGNAM.info | grep "DOWNLOAD="`
if [ ! -e $(basename $DOWNLOAD) ]; then
  echo "Downloading: $DOWNLOAD"
  rm -f $SRCNAM-$VERSION.tar.*
  wget -c $DOWNLOAD
fi

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $WRK $PKG $OUTPUT
cd $WRK
rm -rf $SRCNAM-$VERSION
tar xvf $CWD/$SRCNAM-$VERSION.tar.?z*
cd $SRCNAM-$VERSION

# Make sure ownerships and permissions are sane:
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# Allow building only the BLAS component.
cat $CWD/patches/cmake-piecewise.diff | patch -p1 --verbose

# Configure, build, and install:
# Avoid adding an RPATH entry to the shared lib.  It's unnecessary (except for
# running the test suite), and it's broken on 64-bit (needs LIBDIRSUFFIX).
mkdir -p build
cd build
  cmake \
    -DCMAKE_C_FLAGS="$SLKCFLAGS" \
    -DCMAKE_Fortran_FLAGS="$SLKCFLAGS" \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_RULE_MESSAGES=OFF \
    -DCMAKE_VERBOSE_MAKEFILE=TRUE \
    -DBUILD_BLAS=ON \
    -DBUILD_SHARED_LIBS=ON \
    -DCMAKE_SKIP_RPATH=YES \
    -DCMAKE_BUILD_TYPE=None \
    ..
  make $NUMJOBS
  make install/strip DESTDIR=$PKG
cd -

# Generate man pages.  Requires some fix-ups:
# 0.  Join all escaped newlines so the entire value is replaced.
# 1.  Replace "LAPACK" with "BLAS" in headers/footers.
# 2.  Only generate on the BLAS sources.
# 3.  Turn off recursion.  Only operate on directories in INPUT.
# 4.  Turn off EXCLUDE to not conflict with INPUT.
# 5.  Turn off some repetitive comments.
# 6.  Turn off MAN_LINKS.  See below.
sed -i \
  -e ':a;/\\$/N;s/\\\n//;ta' \
  -e 's/^\(PROJECT_NAME *=\).*/\1 BLAS/' \
  -e 's/^\(INPUT *=\).*/\1 BLAS\/SRC/' \
  -e 's/^\(RECURSIVE *=\).*/\1 NO/' \
  -e 's/^\(EXCLUDE *=\).*/\1/' \
  -e 's/^\(REPEAT_BRIEF *=\).*/\1 NO/' \
  -e 's/^\(MAN_LINKS *=\).*/\1 NO/' \
  DOCS/Doxyfile_man
doxygen DOCS/Doxyfile_man
# Doxygen generates manpages on a file-by-file basis (note the .f extensions).
# We want a page for each function, not each file.  MAN_LINKS creates a page
# for each function that just sources the page for the corresponding file.
# This adds a lot of bloat.  Luckily, functions map 1:1 with files, so we can
# rename .f.3 -> .3 to have the page named after the function.
mkdir -p $PKG/usr/man/man3
for i in DOCS/man/man3/*.f.3; do
  cat $i > $PKG/usr/man/man3/$(basename $i .f.3).3
done

# Don't ship .la files:
rm -f $PKG/{,usr/}lib${LIBDIRSUFFIX}/*.la

# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

# Compress and if needed symlink the man pages:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

# Add a documentation directory.  Not all of these files are expected to be
# present, but listing them ensures that documentation that might appear and
# disappear from version to version will not be missed.
mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a \
  LICENSE README.md \
  $PKG/usr/doc/$PRGNAM-$VERSION

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/${PRGNAM}-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tlz}

echo "Cleaning up build directory"
cd $WRK; rm -rf $SRCNAM-$VERSION $PKG