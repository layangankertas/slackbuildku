#!/bin/sh

# Slackware build script for lz4

# Written by B. Watson (yalhcru@gmail.com)

# Licensed under the WTFPL. See http://www.wtfpl.net/txt/copying/ for details.

# 20191127 bkw: update for 1.9.2
# 20180911 bkw: update for 1.8.3
# 20180611 bkw: update for 1.8.2. don't try to use the updated script
#               to build older versions, the man pages will end up
#               in the wrong place due to upstream changing how they
#               interpret MANDIR
# 20180114 bkw: update for 1.8.1.2. had to add -j1 to the make command.
# 20170819 bkw: update for 1.8.0
# 20170302 bkw: use github long-format URL
# 20170122 bkw: update for 1.7.5
# Modified 2019 by Ali <idnux09@gmail.com>

PRGNAM=lz4
VERSION=$(cat $PRGNAM.info | grep "VERSION" | cut -d = -f 2 | sed 's/"//g')
BUILD=${BUILD:-1}
TAG=${TAG:-_idn}
NUMJOBS=${NUMJOBS:-" -j$(expr $(nproc) + 1) "}

# Automatically determine the architecture we're building on:
MARCH=$( uname -m )
if [ -z "$ARCH" ]; then
  case "$MARCH" in
    i?86)    export ARCH=i586 ;;
    armv7hl) export ARCH=$MARCH ;;
    arm*)    export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
    *)       export ARCH=$MARCH ;;
  esac
fi

CWD=$(pwd)
WRK=${WRK:-/tmp/idn/$PRGNAM}
PKG=$WRK/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

unset DOWNLOAD
eval `cat $PRGNAM.info | grep "DOWNLOAD="`
if [ ! -e $(basename $DOWNLOAD) ]; then
  echo "Downloading: $DOWNLOAD"
  rm -f $PRGNAM-$VERSION.tar.*
  wget -c $DOWNLOAD
fi

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $WRK $PKG $OUTPUT
cd $WRK
rm -rf $PRGNAM-$VERSION
tar xvf $CWD/$PRGNAM-$VERSION.tar.?z* --exclude visual
cd $PRGNAM-$VERSION

# Make sure ownerships and permissions are sane:
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# Configure, build, and install:
# -j1 was necessary in 1.8.1.2. Not sure it's still needed in 1.8.2, but
# lz4 is small & builds pretty fast even with -j1, so I'll leave it for now.
make -j1 \
  CFLAGS="$SLKCFLAGS" \
  CXXFLAGS="$SLKCFLAGS" \
  PREFIX=/usr \
  LIBDIR=/usr/lib$LIBDIRSUFFIX \
  MANDIR=/usr/man \
  DESTDIR=$PKG \
  BUILD_STATIC=no \
  default install

# Don't ship .la files:
rm -f $PKG/{,usr/}lib${LIBDIRSUFFIX}/*.la

# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

# Compress and if needed symlink the man pages:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

# Add a documentation directory.  Not all of these files are expected to be
# present, but listing them ensures that documentation that might appear and
# disappear from version to version will not be missed.
mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a \
  NEWS README* doc/* examples \
  $PKG/usr/doc/$PRGNAM-$VERSION

# the library is BSD licensed, but the programs are GPL.
cat programs/COPYING > $PKG/usr/doc/$PRGNAM-$VERSION/LICENSE.programs.txt
cat lib/LICENSE > $PKG/usr/doc/$PRGNAM-$VERSIONLICENSE.lib.txt

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/${PRGNAM}-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tlz}

echo "Cleaning up build directory"
cd $WRK; rm -rf $PRGNAM-$VERSION $PKG
