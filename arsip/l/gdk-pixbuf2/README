GdkPixbuf is a library that loads image data in various formats and stores it as
linear buffers in memory. The buffers can then be scaled, composited, modified,
saved, or rendered.

GdkPixbuf can load image data encoded in different formats, such as:
- PNG
- JPEG
- TIFF
- TGA
- GIF

Additionally, you can write a GdkPixbuf loader module and install it into a
well-known location, in order to load a file format.

GdkPixbuf is used by the GTK toolkit for loading graphical assets.