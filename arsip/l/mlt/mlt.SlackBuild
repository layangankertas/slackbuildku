#!/bin/sh

# Slackware build script for mlt.

# Written by stormtracknole - stormtracknole@gmail.com
# Modified by Erik Hanson (erik@slackbuilds.org) with
# help from Jonathan Larsen (agentc0re@learnix.net)
# Maintatiner: Edward W. Koenig <kingbeowulf@gmail.com>
#
# Feb 16, 2010 - Modified the script and defined the $PYTHON variable
#	outside of the if statement because it was causing issues with
#	the other if statement that also relies on it's answer.
#	- Fixed the second PYTHON if statement that finds the site package
#	directory.  Added parentheses around the part that cd's into the
#	python swig src to copy the files to the site packages so that it
#	would interfere with the CWD before it
#
# +---------------------------------------+
# Mar 3rd, 2010
#   *Version upgrade to 0.5.2
#	*fixed the "strip" for bash 4
#	 compatability
# +---------------------------------------+
# Sept 12, 2010
#	*Version upgrade to 0.5.6
#	*Bug fixes and enhancements added
# +---------------------------------------+
# May 16, 2011
#	*Version upgrade to 0.7.2
# +---------------------------------------+
# Dec 14, 2011
#	*Version upgrade to 0.7.6
#	*Bug fixes
# +---------------------------------------+
# Jul 2, 2012
#	*Version upgrade to 0.8.0
#	*Bug fixes
# +---------------------------------------+
# Apr 2, 2013
#	*Version upgrade to 0.8.8
#	*Added frei0r as a dependency
# +---------------------------------------+
# Jun 9, 2015
#	*Version upgrade to 0.9.6
#	*Fixed lua portion of the code
#	 thanks to John Vogel.
# +---------------------------------------+
# 20-MAR-2016	new maintainer, version update to 6.0.0
# 11-OCT-2016	version update
# 22-OCT-2016   add qt5 support, bug fixes/addenda (Larry Hajali)
# 29-JAN-2017   version update
# 16-APR-2017   Compiling with vdpau is no longer supported ffmpeg-3.x
# 08-AUG-2018   version update, remove qt5 patch
# 16-APR-2019   version update
# Modified 2019 by Ali <idnux09@gmail.com>

PRGNAM=mlt
VERSION=$(cat $PRGNAM.info | grep "VERSION" | cut -d = -f 2 | sed 's/"//g')
BUILD=${BUILD:-3}
TAG=${TAG:-_idn}
NUMJOBS=${NUMJOBS:-" -j$(expr $(nproc) + 1) "}

# Automatically determine the architecture we're building on:
MARCH=$( uname -m )
if [ -z "$ARCH" ]; then
  case "$MARCH" in
    i?86)    export ARCH=i586 ;;
    armv7hl) export ARCH=$MARCH ;;
    arm*)    export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
    *)       export ARCH=$MARCH ;;
  esac
fi

CWD=$(pwd)
WRK=${WRK:-/tmp/idn/$PRGNAM}
PKG=$WRK/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

# Download source files:
unset DOWNLOAD
unset MD5SUM
eval `cat $PRGNAM.info | grep "DOWNLOAD="`
eval `cat $PRGNAM.info | grep "MD5SUM="`
if [ ! -e $(basename $DOWNLOAD) ]; then
  echo "Downloading: $DOWNLOAD"
  wget -c $DOWNLOAD
fi
while [ $(md5sum $(basename $DOWNLOAD) | cut -d " " -f 1) != $MD5SUM ] ; do
  echo "Downloading: $DOWNLOAD"
  wget -c $DOWNLOAD
done

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $WRK $PKG $OUTPUT
cd $WRK
rm -rf $PRGNAM-$VERSION
tar xvf $CWD/$PRGNAM-$VERSION.tar.?z*
cd $PRGNAM-$VERSION

# Make sure ownerships and permissions are sane:
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# Apply patches:
cat $CWD/patches/7063e88e.patch | patch -p1 --verbose
cat $CWD/patches/a87229bc.patch | patch -p1 --verbose

# Configure, build, and install:
mkdir -p build
cd build
  cmake \
    -DCMAKE_C_FLAGS="$SLKCFLAGS" \
    -DCMAKE_CXX_FLAGS="$SLKCFLAGS" \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=/usr/lib${LIBDIRSUFFIX} \
    -DCMAKE_INSTALL_MANDIR=/usr/man \
    -DGPL=ON \
    -DGPL3=ON \
    -DMOD_AVFORMAT=ON \
    -DMOD_FREI0R=ON \
    -DMOD_JACKRACK=ON \
    -DMOD_NORMALIZE=ON \
    -DMOD_OPENCV=ON \
    -DMOD_OPENGL=ON \
    -DMOD_PLUS=ON \
    -DMOD_PLUSGPL=ON \
    -DMOD_QT=ON \
    -DMOD_RTAUDIO=OFF \
    -DSWIG_PYTHON=ON \
    -DBUILD_DOCS=ON \
    -DCMAKE_BUILD_TYPE=Release \
    ..
  make $NUMJOBS
  make install DESTDIR=$PKG
cd -

# Don't ship .la files:
rm -f $PKG/{,usr/}lib${LIBDIRSUFFIX}/*.la

# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

# Compress info files:
if [ -d $PKG/usr/info ]; then
  ( cd $PKG/usr/info
    rm -f dir
    gzip -9 *
  )
fi

# Compress and if needed symlink the man pages:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

# Add a documentation directory.  Not all of these files are expected to be
# present, but listing them ensures that documentation that might appear and
# disappear from version to version will not be missed.
mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a \
  AUTHORS NEWS COPYING README GPL* \
  $PKG/usr/doc/$PRGNAM-$VERSION

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/${PRGNAM}-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tlz}

echo "Cleaning up build directory"
cd $WRK; rm -rf $PRGNAM-$VERSION $PKG