#!/bin/sh

# Slackware build script for Qbs

# Copyright 2014-2017 Nikos Giotis <nikos.giotis@mail.com>
# Copyright 2019 Andrzej Telszewski, Szczecin
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# Modified 2019 by Ali <idnux09@gmail.com>

PRGNAM=qbs
VERSION=$(cat $PRGNAM.info | grep "VERSION" | cut -d = -f 2 | sed 's/"//g')
BUILD=${BUILD:-1}
TAG=${TAG:-_idn}
NUMJOBS=${NUMJOBS:-" -j$(expr $(nproc) + 1) "}

# Automatically determine the architecture we're building on:
MARCH=$( uname -m )
if [ -z "$ARCH" ]; then
  case "$MARCH" in
    i?86)    export ARCH=i586 ;;
    armv7hl) export ARCH=$MARCH ;;
    arm*)    export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
    *)       export ARCH=$MARCH ;;
  esac
fi

CWD=$(pwd)
WRK=${WRK:-/tmp/idn/$PRGNAM}
PKG=$WRK/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

# Download source files:
unset DOWNLOAD
unset MD5SUM
eval `cat $PRGNAM.info | grep "DOWNLOAD="`
eval `cat $PRGNAM.info | grep "MD5SUM="`
if [ ! -e $(basename $DOWNLOAD) ]; then
  echo "Downloading: $DOWNLOAD"
  wget -c $DOWNLOAD
fi
while [ $(md5sum $(basename $DOWNLOAD) | cut -d " " -f 1) != $MD5SUM ] ; do
  echo "Downloading: $DOWNLOAD"
  wget -c $DOWNLOAD
done

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $WRK $PKG $OUTPUT
cd $WRK
echo "Removing any existing source directory. Please wait..."
rm -rf $PRGNAM-src-$VERSION
tar xvf $CWD/$PRGNAM-src-$VERSION.tar.?z*
cd $PRGNAM-src-$VERSION

# Make sure ownerships and permissions are sane:
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# Configure, build, and install:
mkdir -p build
cd build
  cmake \
    -GNinja \
    -DCMAKE_C_FLAGS="$SLKCFLAGS" \
    -DCMAKE_CXX_FLAGS="$SLKCFLAGS" \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DQBS_LIB_INSTALL_DIR=/usr/lib${LIBDIRSUFFIX} \
    -DQBS_DOC_INSTALL_DIR=/usr/doc/$PRGNAM-$VERSION \
    -DQBS_PLUGINS_INSTALL_BASE=lib${LIBDIRSUFFIX} \
    -DQBS_INSTALL_HTML_DOCS=ON \
    -DQBS_INSTALL_QCH_DOCS=ON \
    -DWITH_TESTS=ON \
    -DQBS_ENABLE_RPATH=OFF \
    -DBUILD_SHARED_LIBS=ON \
    -DCMAKE_BUILD_TYPE=Release \
    ..
  ninja $NUMJOBS
  DESTDIR=$PKG ninja install
cd -

# And the .desktop files:
mkdir -p $PKG/usr/share/applications
cat <<EOF > $PKG/usr/share/applications/$PRGNAM-config-ui.desktop
[Desktop Entry]
Name=Qbs Settings
Comment=Qbs Configuration UI
Exec=qbs-config-ui
Icon=qt-logo
Terminal=false
Type=Application
Categories=Qt;Development;
EOF

# Install man pages:
mkdir -p $PKG/usr/man/man1
cp doc/man/qbs.1 $PKG/usr/man/man1

# Don't ship .la files:
rm -f $PKG/{,usr/}lib${LIBDIRSUFFIX}/*.la

# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

# Compress info files:
if [ -d $PKG/usr/info ]; then
  ( cd $PKG/usr/info
    rm -f dir
    gzip -9 *
  )
fi

# Compress and if needed symlink the man pages:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

# Add a documentation directory.  Not all of these files are expected to be
# present, but listing them ensures that documentation that might appear and
# disappear from version to version will not be missed.
mkdir -p $PKG/usr/doc/{$PRGNAM-$VERSION,qt6}
cp -a \
  LGPL_EXCEPTION.txt LICENSE.GPL3-EXCEPT \
  LICENSE.{LGPLv21,LGPLv3} README.md \
  $PKG/usr/doc/$PRGNAM-$VERSION
mv $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.qch $PKG/usr/doc/qt6

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/${PRGNAM}-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh  > $PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tlz}

echo "Cleaning up build directory"
cd $WRK; rm -rf $PRGNAM-src-$VERSION $PKG