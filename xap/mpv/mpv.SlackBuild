#!/bin/sh

# Slackware build script for mpv

# Copyright 2015 John Vogel Corning, NY USA
# Copyright 2017,2018 (versions 0.25.0-0.29.1) Andreas Guldstrand
# Copyright 2020 Christoph Willing  Brisbane Australia
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# Modified 2021 by Ali <idnux09@gmail.com>

PRGNAM=mpv
VERSION=$(cat $PRGNAM.info | grep "VERSION" | cut -d = -f 2 | sed 's/"//g')
BUILD=${BUILD:-1}
TAG=${TAG:-_idn}
NUMJOBS=${NUMJOBS:-" -j$(expr $(nproc) + 1) "}

# Automatically determine the architecture we're building on:
MARCH=$( uname -m )
if [ -z "$ARCH" ]; then
  case "$MARCH" in
    i?86)    export ARCH=i586 ;;
    armv7hl) export ARCH=$MARCH ;;
    arm*)    export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
    *)       export ARCH=$MARCH ;;
  esac
fi

CWD=$(pwd)
WRK=${WRK:-/tmp/idn/$PRGNAM}
PKG=$WRK/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

# Download source files:
unset DOWNLOAD
unset MD5SUM
eval `cat $PRGNAM.info | grep "DOWNLOAD="`
eval `cat $PRGNAM.info | grep "MD5SUM="`
if [ ! -e $(basename $DOWNLOAD) ]; then
  echo "Downloading: $DOWNLOAD"
  wget -c $DOWNLOAD
fi
while [ $(md5sum $(basename $DOWNLOAD) | cut -d " " -f 1) != $MD5SUM ] ; do
  echo "Downloading: $DOWNLOAD"
  wget -c $DOWNLOAD
done

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $WRK $PKG $OUTPUT
cd $WRK
echo "Removing any existing source directory. Please wait..."
rm -rf $PRGNAM-$VERSION
tar xvf $CWD/$PRGNAM-$VERSION.tar.?z*
cd $PRGNAM-$VERSION

# Make sure ownerships and permissions are sane:
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# Enable VDPAU support for Nvidia GPU:
if pkg-config --exists vdpau ; then
  vdpau="-Dvdpau=enabled"
else
  vdpau="-Dvdpau=disabled"
fi

# Enable CUDA support for Nvidia GPU:
if pkg-config --exists cuda ; then
  cuda="-Dcuda-hwaccel=enabled"
else
  cuda="-Dcuda-hwaccel=disabled"
fi

# Apply patches:
cat $CWD/patches/confdir.patch | patch -p1 --verbose

# Configure, build, and install:
mkdir -p build
cd build
  CFLAGS="$SLKCFLAGS" \
  CXXFLAGS="$SLKCFLAGS" \
  meson setup \
    --prefix=/usr \
    --libdir=/usr/lib${LIBDIRSUFFIX} \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --infodir=/usr/info \
    --mandir=/usr/man \
    -Dlibmpv=false \
    -Dcplayer=true \
    -Dgpl=true\
    -Djavascript=enabled \
    -Dlcms2=enabled \
    -Dlua=enabled \
    -Dalsa=enabled \
    -Djack=enabled \
    -Dopenal=enabled \
    -Dpipewire=enabled \
    -Dpulse=enabled \
    -Dgbm=enabled \
    -Degl=enabled \
    -Degl-wayland=enabled \
    -Degl-x11=enabled \
    -Dgl=enabled \
    -Drubberband=enabled \
    -Dsdl2=enabled \
    -Dsdl2-video=enabled \
    -Dshaderc=disabled \
    -Dspirv-cross=disabled \
    -Dvaapi=enabled \
    -Dvulkan=enabled \
    -Dwayland=enabled \
    -Dx11=enabled \
    $vdpau \
    $cuda \
    -Dmanpage-build=enabled \
    --buildtype=release \
    ..
  ninja $NUMJOBS
  DESTDIR=$PKG ninja install
cd -

# Preserve old config
for f in $PKG/etc/mpv/*.conf ; do
  mv $f{,.new}
done

# Don't ship .la files:
rm -f $PKG/{,usr/}lib${LIBDIRSUFFIX}/*.la

# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

# Compress info files:
if [ -d $PKG/usr/info ]; then
  ( cd $PKG/usr/info
    rm -f dir
    gzip -9 *
  )
fi

# Compress and if needed symlink the man pages:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

# Add a documentation directory.  Not all of these files are expected to be
# present, but listing them ensures that documentation that might appear and
# disappear from version to version will not be missed.
mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a \
  Copyright LICENSE* README.md RELEASE_NOTES MPV_VERSION \
  $PKG/usr/doc/$PRGNAM-$VERSION

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION/manual
for f in DOCS/*.md DOCS/*.rst DOCS/*.txt; do
  cp -a $f $PKG/usr/doc/$PRGNAM-$VERSION/manual/
done

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/${PRGNAM}-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -p -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tlz}

echo "Cleaning up build directory"
cd $WRK; rm -rf $PRGNAM-$VERSION $PKG