if [ -x /usr/bin/update-desktop-database ]; then
  /usr/bin/update-desktop-database -q usr/share/applications >/dev/null 2>&1
fi

if [ -x /usr/bin/update-mime-database ]; then
  /usr/bin/update-mime-database usr/share/mime >/dev/null 2>&1
fi

if [ -e usr/share/icons/hicolor/icon-theme.cache ]; then
  if [ -x /usr/bin/gtk-update-icon-cache ]; then
    /usr/bin/gtk-update-icon-cache -f usr/share/icons/hicolor >/dev/null 2>&1
  fi
fi

config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}

config etc/pcb-rnd/adialogs.conf.new
config etc/pcb-rnd/ar_extern.conf.new
config etc/pcb-rnd/asm.conf.new
config etc/pcb-rnd/cam.conf.new
config etc/pcb-rnd/ch_editpoint.conf.new
config etc/pcb-rnd/conf_core.lht.new
config etc/pcb-rnd/drc_query.conf.new
config etc/pcb-rnd/export_bom.conf.new
config etc/pcb-rnd/export_xy.conf.new
config etc/pcb-rnd/fp_fs.conf.new
config etc/pcb-rnd/fp_wget.conf.new
config etc/pcb-rnd/import_gnetlist.conf.new
config etc/pcb-rnd/import_sch_rnd.conf.new
config etc/pcb-rnd/io_pads.conf.new
config etc/pcb-rnd/menu-default.lht.new
config etc/pcb-rnd/order.conf.new
config etc/pcb-rnd/order_pcbway.conf.new
config etc/pcb-rnd/show_netnames.conf.new
