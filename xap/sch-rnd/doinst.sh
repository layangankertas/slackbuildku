if [ -x /usr/bin/update-desktop-database ]; then
  /usr/bin/update-desktop-database -q usr/share/applications >/dev/null 2>&1
fi

if [ -x /usr/bin/update-mime-database ]; then
  /usr/bin/update-mime-database usr/share/mime >/dev/null 2>&1
fi

if [ -e usr/share/icons/hicolor/icon-theme.cache ]; then
  if [ -x /usr/bin/gtk-update-icon-cache ]; then
    /usr/bin/gtk-update-icon-cache -f usr/share/icons/hicolor >/dev/null 2>&1
  fi
fi

config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}

config etc/sch-rnd/adialogs.conf.new
config etc/sch-rnd/export_bom.conf.new
config etc/sch-rnd/funcmap.conf.new
config etc/sch-rnd/io_altium.conf.new
config etc/sch-rnd/io_geda.conf.new
config etc/sch-rnd/io_orcad.conf.new
config etc/sch-rnd/io_tinycad.conf.new
config etc/sch-rnd/menu-default.lht.new
config etc/sch-rnd/renumber.conf.new
config etc/sch-rnd/sch-rnd-conf.lht.new
config etc/sch-rnd/sim.conf.new
config etc/sch-rnd/sim_gui.conf.new
config etc/sch-rnd/std_cschem.conf.new
config etc/sch-rnd/std_devmap.conf.new
config etc/sch-rnd/target_pcb.conf.new
config etc/sch-rnd/target_spice.conf.new
