From 3aa1fb33a4027d20a9acd720a56efb0ab620769c Mon Sep 17 00:00:00 2001
From: Neal Gompa <neal@gompa.dev>
Date: Sat, 7 Jan 2023 23:15:13 -0500
Subject: [PATCH 1/2] UI: Consistently reference the software H264 encoder
 properly

The code here assumes that the only software encoder is the x264-based
H.264 encoder. That may not always remain true. This change adjusts
the encoder string to indicate that it's an H.264 encoder from x264.
---
 UI/data/locale/en-US.ini             | 4 ++--
 UI/window-basic-auto-config-test.cpp | 6 +++---
 UI/window-basic-settings-stream.cpp  | 2 +-
 UI/window-basic-settings.cpp         | 4 ++--
 4 files changed, 8 insertions(+), 8 deletions(-)

diff --git a/UI/data/locale/en-US.ini b/UI/data/locale/en-US.ini
index d1d4b2fd2c27a..b29df44558055 100644
--- a/UI/data/locale/en-US.ini
+++ b/UI/data/locale/en-US.ini
@@ -997,7 +997,7 @@ Basic.Settings.Output.Simple.Warn.Encoder="Warning: Recording with a software en
 Basic.Settings.Output.Simple.Warn.Lossless="Warning: Lossless quality generates tremendously large file sizes! Lossless quality can use upward of 7 gigabytes of disk space per minute at high resolutions and framerates. Lossless is not recommended for long recordings unless you have a very large amount of disk space available. Replay buffer is unavailable when using lossless quality."
 Basic.Settings.Output.Simple.Warn.Lossless.Msg="Are you sure you want to use lossless quality?"
 Basic.Settings.Output.Simple.Warn.Lossless.Title="Lossless quality warning!"
-Basic.Settings.Output.Simple.Encoder.Software="Software (x264)"
+Basic.Settings.Output.Simple.Encoder.Software.X264="Software (x264, H.264)"
 Basic.Settings.Output.Simple.Encoder.Hardware.QSV.H264="Hardware (QSV, H.264)"
 Basic.Settings.Output.Simple.Encoder.Hardware.QSV.AV1="Hardware (QSV, AV1)"
 Basic.Settings.Output.Simple.Encoder.Hardware.AMD.H264="Hardware (AMD, H.264)"
@@ -1008,7 +1008,7 @@ Basic.Settings.Output.Simple.Encoder.Hardware.NVENC.AV1="Hardware (NVENC, AV1)"
 Basic.Settings.Output.Simple.Encoder.Hardware.NVENC.HEVC="Hardware (NVENC, HEVC)"
 Basic.Settings.Output.Simple.Encoder.Hardware.Apple.H264="Hardware (Apple, H.264)"
 Basic.Settings.Output.Simple.Encoder.Hardware.Apple.HEVC="Hardware (Apple, HEVC)"
-Basic.Settings.Output.Simple.Encoder.SoftwareLowCPU="Software (x264 low CPU usage preset, increases file size)"
+Basic.Settings.Output.Simple.Encoder.SoftwareLowCPU.X264="Software (x264 H.264 low CPU usage preset, increases file size)"
 Basic.Settings.Output.Simple.Codec.AAC="AAC"
 Basic.Settings.Output.Simple.Codec.AAC.Default="AAC (Default)"
 Basic.Settings.Output.Simple.Codec.Opus="Opus"
diff --git a/UI/window-basic-auto-config-test.cpp b/UI/window-basic-auto-config-test.cpp
index 7d7c4278ede57..2a071a6052c3a 100644
--- a/UI/window-basic-auto-config-test.cpp
+++ b/UI/window-basic-auto-config-test.cpp
@@ -1010,7 +1010,7 @@ void AutoConfigTestPage::TestRecordingEncoderThread()
 }
 
 #define ENCODER_TEXT(x) "Basic.Settings.Output.Simple.Encoder." x
-#define ENCODER_SOFTWARE ENCODER_TEXT("Software")
+#define ENCODER_X264 ENCODER_TEXT("Software.X264")
 #define ENCODER_NVENC ENCODER_TEXT("Hardware.NVENC.H264")
 #define ENCODER_QSV ENCODER_TEXT("Hardware.QSV.H264")
 #define ENCODER_AMD ENCODER_TEXT("Hardware.AMD.H264")
@@ -1050,7 +1050,7 @@ void AutoConfigTestPage::FinalizeResults()
 	auto encName = [](AutoConfig::Encoder enc) -> QString {
 		switch (enc) {
 		case AutoConfig::Encoder::x264:
-			return QTStr(ENCODER_SOFTWARE);
+			return QTStr(ENCODER_X264);
 		case AutoConfig::Encoder::NVENC:
 			return QTStr(ENCODER_NVENC);
 		case AutoConfig::Encoder::QSV:
@@ -1063,7 +1063,7 @@ void AutoConfigTestPage::FinalizeResults()
 			return QTStr(QUALITY_SAME);
 		}
 
-		return QTStr(ENCODER_SOFTWARE);
+		return QTStr(ENCODER_X264);
 	};
 
 	auto newLabel = [this](const char *str) -> QLabel * {
diff --git a/UI/window-basic-settings-stream.cpp b/UI/window-basic-settings-stream.cpp
index 6c683ddb78222..2c7ff637bc703 100644
--- a/UI/window-basic-settings-stream.cpp
+++ b/UI/window-basic-settings-stream.cpp
@@ -1673,7 +1673,7 @@ void OBSBasicSettings::ResetEncoders(bool streamOnly)
 
 #define ENCODER_STR(str) QTStr("Basic.Settings.Output.Simple.Encoder." str)
 
-	ui->simpleOutStrEncoder->addItem(ENCODER_STR("Software"),
+	ui->simpleOutStrEncoder->addItem(ENCODER_STR("Software.X264"),
 					 QString(SIMPLE_ENCODER_X264));
 #ifdef _WIN32
 	if (service_supports_encoder(vcodecs, "obs_qsv11"))
diff --git a/UI/window-basic-settings.cpp b/UI/window-basic-settings.cpp
index f1fad879b0033..ba706380bd65d 100644
--- a/UI/window-basic-settings.cpp
+++ b/UI/window-basic-settings.cpp
@@ -5322,9 +5322,9 @@ void OBSBasicSettings::FillSimpleRecordingValues()
 	ADD_QUALITY("HQ");
 	ADD_QUALITY("Lossless");
 
-	ui->simpleOutRecEncoder->addItem(ENCODER_STR("Software"),
+	ui->simpleOutRecEncoder->addItem(ENCODER_STR("Software.X264"),
 					 QString(SIMPLE_ENCODER_X264));
-	ui->simpleOutRecEncoder->addItem(ENCODER_STR("SoftwareLowCPU"),
+	ui->simpleOutRecEncoder->addItem(ENCODER_STR("SoftwareLowCPU.X264"),
 					 QString(SIMPLE_ENCODER_X264_LOWCPU));
 	if (EncoderAvailable("obs_qsv11"))
 		ui->simpleOutRecEncoder->addItem(

From a29a323d14e36d46c34a00566785efb267ceae4d Mon Sep 17 00:00:00 2001
From: Neal Gompa <neal@gompa.dev>
Date: Sun, 26 Mar 2023 06:06:31 -0400
Subject: [PATCH 2/2] obs-ffmpeg: Add initial support for the OpenH264 H.264
 software codec

This allows users to leverage the OpenH264 codec from Cisco to encode
H.264 video content. It is significantly reduced in capability from
alternatives, but it does the job.

This also provides a framework for adding support for other H.264
software codecs provided through FFmpeg.
---
 plugins/obs-ffmpeg/CMakeLists.txt        |   1 +
 plugins/obs-ffmpeg/cmake/legacy.cmake    |   1 +
 plugins/obs-ffmpeg/data/locale/en-US.ini |   3 +
 plugins/obs-ffmpeg/obs-ffmpeg-h264.c     | 260 +++++++++++++++++++++++
 plugins/obs-ffmpeg/obs-ffmpeg.c          |   2 +
 5 files changed, 267 insertions(+)
 create mode 100644 plugins/obs-ffmpeg/obs-ffmpeg-h264.c

diff --git a/plugins/obs-ffmpeg/CMakeLists.txt b/plugins/obs-ffmpeg/CMakeLists.txt
index 77c5ad8e4950b..81b847889ef4a 100644
--- a/plugins/obs-ffmpeg/CMakeLists.txt
+++ b/plugins/obs-ffmpeg/CMakeLists.txt
@@ -32,6 +32,7 @@ target_sources(
           obs-ffmpeg-av1.c
           obs-ffmpeg-compat.h
           obs-ffmpeg-formats.h
+          obs-ffmpeg-h264.c
           obs-ffmpeg-hls-mux.c
           obs-ffmpeg-mux.c
           obs-ffmpeg-mux.h
diff --git a/plugins/obs-ffmpeg/cmake/legacy.cmake b/plugins/obs-ffmpeg/cmake/legacy.cmake
index cf92a9ea58d7e..907a2f2878d4a 100644
--- a/plugins/obs-ffmpeg/cmake/legacy.cmake
+++ b/plugins/obs-ffmpeg/cmake/legacy.cmake
@@ -40,6 +40,7 @@ target_sources(
           obs-ffmpeg-video-encoders.c
           obs-ffmpeg-audio-encoders.c
           obs-ffmpeg-av1.c
+          obs-ffmpeg-h264.c
           obs-ffmpeg-nvenc.c
           obs-ffmpeg-output.c
           obs-ffmpeg-output.h
diff --git a/plugins/obs-ffmpeg/data/locale/en-US.ini b/plugins/obs-ffmpeg/data/locale/en-US.ini
index 9652be1600d24..e18e39f5aaf21 100644
--- a/plugins/obs-ffmpeg/data/locale/en-US.ini
+++ b/plugins/obs-ffmpeg/data/locale/en-US.ini
@@ -120,4 +120,7 @@ NVENC.CheckDrivers="Try installing the latest <a href=\"https://obsproject.com/g
 
 AV1.8bitUnsupportedHdr="OBS does not support 8-bit output of Rec. 2100."
 
+H264.UnsupportedVideoFormat="Only video formats using 8-bit color are supported."
+H264.UnsupportedColorSpace="Only the Rec. 709 color space is supported."
+
 ReconnectDelayTime="Reconnect Delay"
diff --git a/plugins/obs-ffmpeg/obs-ffmpeg-h264.c b/plugins/obs-ffmpeg/obs-ffmpeg-h264.c
new file mode 100644
index 0000000000000..179a61ccf22eb
--- /dev/null
+++ b/plugins/obs-ffmpeg/obs-ffmpeg-h264.c
@@ -0,0 +1,260 @@
+/******************************************************************************
+    Copyright (C) 2023 by Neal Gompa <neal@gompa.dev>
+    Partly derived from obs-ffmpeg-av1.c by Hugh Bailey <obs.jim@gmail.com>
+
+    This program is free software: you can redistribute it and/or modify
+    it under the terms of the GNU General Public License as published by
+    the Free Software Foundation, either version 2 of the License, or
+    (at your option) any later version.
+
+    This program is distributed in the hope that it will be useful,
+    but WITHOUT ANY WARRANTY; without even the implied warranty of
+    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+    GNU General Public License for more details.
+
+    You should have received a copy of the GNU General Public License
+    along with this program.  If not, see <http://www.gnu.org/licenses/>.
+******************************************************************************/
+
+#include "obs-ffmpeg-video-encoders.h"
+
+#define do_log(level, format, ...)                   \
+	blog(level, "[H.264 encoder: '%s'] " format, \
+	     obs_encoder_get_name(enc->ffve.encoder), ##__VA_ARGS__)
+
+#define error(format, ...) do_log(LOG_ERROR, format, ##__VA_ARGS__)
+#define warn(format, ...) do_log(LOG_WARNING, format, ##__VA_ARGS__)
+#define info(format, ...) do_log(LOG_INFO, format, ##__VA_ARGS__)
+#define debug(format, ...) do_log(LOG_DEBUG, format, ##__VA_ARGS__)
+
+enum h264_encoder_type {
+	H264_ENCODER_TYPE_OH264,
+};
+
+struct h264_encoder {
+	struct ffmpeg_video_encoder ffve;
+	enum h264_encoder_type type;
+
+	DARRAY(uint8_t) header;
+};
+
+static const char *oh264_getname(void *unused)
+{
+	UNUSED_PARAMETER(unused);
+	return "OpenH264";
+}
+
+static void h264_video_info(void *data, struct video_scale_info *info)
+{
+	UNUSED_PARAMETER(data);
+
+	// OpenH264 only supports I420
+	info->format = VIDEO_FORMAT_I420;
+}
+
+static bool h264_update(struct h264_encoder *enc, obs_data_t *settings)
+{
+	const char *profile = obs_data_get_string(settings, "profile");
+	int bitrate = (int)obs_data_get_int(settings, "bitrate");
+	int keyint_sec = 0;              // This is not supported by OpenH264
+	const char *rc_mode = "quality"; // We only want to use quality mode
+	int allow_skip_frames = 1;       // This is required for quality mode
+
+	video_t *video = obs_encoder_video(enc->ffve.encoder);
+	const struct video_output_info *voi = video_output_get_info(video);
+	struct video_scale_info info;
+
+	info.format = voi->format;
+	info.colorspace = voi->colorspace;
+	info.range = voi->range;
+
+	enc->ffve.context->thread_count = 0;
+
+	h264_video_info(enc, &info);
+
+	av_opt_set(enc->ffve.context->priv_data, "rc_mode", rc_mode, 0);
+	av_opt_set(enc->ffve.context->priv_data, "profile", profile, 0);
+	av_opt_set_int(enc->ffve.context->priv_data, "allow_skip_frames",
+		       allow_skip_frames, 0);
+
+	const char *ffmpeg_opts = obs_data_get_string(settings, "ffmpeg_opts");
+	ffmpeg_video_encoder_update(&enc->ffve, bitrate, keyint_sec, voi, &info,
+				    ffmpeg_opts);
+	info("settings:\n"
+	     "\tencoder:      %s\n"
+	     "\trc_mode:      %s\n"
+	     "\tbitrate:      %d\n"
+	     "\tprofile:      %s\n"
+	     "\twidth:        %d\n"
+	     "\theight:       %d\n"
+	     "\tffmpeg opts:  %s\n",
+	     enc->ffve.enc_name, rc_mode, bitrate, profile,
+	     enc->ffve.context->width, enc->ffve.height, ffmpeg_opts);
+
+	enc->ffve.context->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
+	return ffmpeg_video_encoder_init_codec(&enc->ffve);
+}
+
+static void h264_destroy(void *data)
+{
+	struct h264_encoder *enc = data;
+
+	ffmpeg_video_encoder_free(&enc->ffve);
+	da_free(enc->header);
+	bfree(enc);
+}
+
+static void on_first_packet(void *data, AVPacket *pkt, struct darray *da)
+{
+	struct h264_encoder *enc = data;
+
+	da_copy_array(enc->header, enc->ffve.context->extradata,
+		      enc->ffve.context->extradata_size);
+
+	darray_copy_array(1, da, pkt->data, pkt->size);
+}
+
+static void *h264_create_internal(obs_data_t *settings, obs_encoder_t *encoder,
+				  const char *enc_lib, const char *enc_name)
+{
+	video_t *video = obs_encoder_video(encoder);
+	const struct video_output_info *voi = video_output_get_info(video);
+
+	switch (voi->format) {
+	// planar 4:2:0 formats
+	case VIDEO_FORMAT_I420: // three-plane
+	case VIDEO_FORMAT_NV12: // two-plane, luma and packed chroma
+	// packed 4:2:2 formats
+	case VIDEO_FORMAT_YVYU:
+	case VIDEO_FORMAT_YUY2: // YUYV
+	case VIDEO_FORMAT_UYVY:
+	// packed uncompressed formats
+	case VIDEO_FORMAT_RGBA:
+	case VIDEO_FORMAT_BGRA:
+	case VIDEO_FORMAT_BGRX:
+	case VIDEO_FORMAT_BGR3:
+	case VIDEO_FORMAT_Y800: // grayscale
+	// planar 4:4:4
+	case VIDEO_FORMAT_I444:
+	// planar 4:2:2
+	case VIDEO_FORMAT_I422:
+	// planar 4:2:0 with alpha
+	case VIDEO_FORMAT_I40A:
+	// planar 4:2:2 with alpha
+	case VIDEO_FORMAT_I42A:
+	// planar 4:4:4 with alpha
+	case VIDEO_FORMAT_YUVA:
+	// packed 4:4:4 with alpha
+	case VIDEO_FORMAT_AYUV:
+		break;
+	default:; // Make the compiler do the right thing
+		const char *const text =
+			obs_module_text("H264.UnsupportedVideoFormat");
+		obs_encoder_set_last_error(encoder, text);
+		blog(LOG_ERROR, "[H.264 encoder] %s", text);
+		return NULL;
+	}
+
+	switch (voi->colorspace) {
+	case VIDEO_CS_DEFAULT:
+	case VIDEO_CS_709:
+		break;
+	default:; // Make the compiler do the right thing
+		const char *const text =
+			obs_module_text("H264.UnsupportedColorSpace");
+		obs_encoder_set_last_error(encoder, text);
+		blog(LOG_ERROR, "[H.264 encoder] %s", text);
+		return NULL;
+	}
+
+	struct h264_encoder *enc = bzalloc(sizeof(*enc));
+
+	if (strcmp(enc_lib, "libopenh264") == 0)
+		enc->type = H264_ENCODER_TYPE_OH264;
+
+	if (!ffmpeg_video_encoder_init(&enc->ffve, enc, encoder, enc_lib, NULL,
+				       enc_name, NULL, on_first_packet))
+		goto fail;
+	if (!h264_update(enc, settings))
+		goto fail;
+
+	return enc;
+
+fail:
+	h264_destroy(enc);
+	return NULL;
+}
+
+static void *oh264_create(obs_data_t *settings, obs_encoder_t *encoder)
+{
+	return h264_create_internal(settings, encoder, "libopenh264",
+				    "OpenH264");
+}
+
+static bool h264_encode(void *data, struct encoder_frame *frame,
+			struct encoder_packet *packet, bool *received_packet)
+{
+	struct h264_encoder *enc = data;
+	return ffmpeg_video_encode(&enc->ffve, frame, packet, received_packet);
+}
+
+void h264_defaults(obs_data_t *settings)
+{
+	obs_data_set_default_int(settings, "bitrate", 2500);
+	obs_data_set_default_string(settings, "profile", "main");
+}
+
+obs_properties_t *h264_properties(enum h264_encoder_type type)
+{
+	UNUSED_PARAMETER(type); // Only one encoder right now...
+	obs_properties_t *props = obs_properties_create();
+	obs_property_t *p;
+
+	p = obs_properties_add_list(props, "profile",
+				    obs_module_text("Profile"),
+				    OBS_COMBO_TYPE_LIST,
+				    OBS_COMBO_FORMAT_STRING);
+	obs_property_list_add_string(p, "constrained_baseline",
+				     "constrained_baseline");
+	obs_property_list_add_string(p, "main", "main");
+	obs_property_list_add_string(p, "high", "high");
+
+	p = obs_properties_add_int(props, "bitrate", obs_module_text("Bitrate"),
+				   50, 300000, 50);
+	obs_property_int_set_suffix(p, " Kbps");
+
+	obs_properties_add_text(props, "ffmpeg_opts",
+				obs_module_text("FFmpegOpts"),
+				OBS_TEXT_DEFAULT);
+
+	return props;
+}
+
+obs_properties_t *oh264_properties(void *unused)
+{
+	UNUSED_PARAMETER(unused);
+	return h264_properties(H264_ENCODER_TYPE_OH264);
+}
+
+static bool h264_extra_data(void *data, uint8_t **extra_data, size_t *size)
+{
+	struct h264_encoder *enc = data;
+
+	*extra_data = enc->header.array;
+	*size = enc->header.num;
+	return true;
+}
+
+struct obs_encoder_info oh264_encoder_info = {
+	.id = "ffmpeg_openh264",
+	.type = OBS_ENCODER_VIDEO,
+	.codec = "h264",
+	.get_name = oh264_getname,
+	.create = oh264_create,
+	.destroy = h264_destroy,
+	.encode = h264_encode,
+	.get_defaults = h264_defaults,
+	.get_properties = oh264_properties,
+	.get_extra_data = h264_extra_data,
+	.get_video_info = h264_video_info,
+};
diff --git a/plugins/obs-ffmpeg/obs-ffmpeg.c b/plugins/obs-ffmpeg/obs-ffmpeg.c
index bca4ca1153988..a75625b32f741 100644
--- a/plugins/obs-ffmpeg/obs-ffmpeg.c
+++ b/plugins/obs-ffmpeg/obs-ffmpeg.c
@@ -37,6 +37,7 @@ extern struct obs_encoder_info pcm24_encoder_info;
 extern struct obs_encoder_info pcm32_encoder_info;
 extern struct obs_encoder_info alac_encoder_info;
 extern struct obs_encoder_info flac_encoder_info;
+extern struct obs_encoder_info oh264_encoder_info;
 extern struct obs_encoder_info h264_nvenc_encoder_info;
 #ifdef ENABLE_HEVC
 extern struct obs_encoder_info hevc_nvenc_encoder_info;
@@ -366,6 +367,7 @@ bool obs_module_load(void)
 	obs_register_output(&ffmpeg_hls_muxer);
 	obs_register_output(&replay_buffer);
 	obs_register_encoder(&aac_encoder_info);
+	register_encoder_if_available(&oh264_encoder_info, "libopenh264");
 	register_encoder_if_available(&svt_av1_encoder_info, "libsvtav1");
 	register_encoder_if_available(&aom_av1_encoder_info, "libaom-av1");
 	obs_register_encoder(&opus_encoder_info);
