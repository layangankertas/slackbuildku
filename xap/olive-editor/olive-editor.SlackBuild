#!/bin/sh

# Slackware build script for olive-editor

# Copyright 2023 Ali, <idnux09@gmail.com>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

PRGNAM=olive-editor
SRCNAM=olive
VERSION=$(cat $PRGNAM.info | grep "VERSION" | cut -d = -f 2 | sed 's/"//g')
COMMIT=$(cat $PRGNAM.info | grep "SRC_COMMIT" | cut -d = -f 2 | sed 's/"//g')
CORE_COMMIT=$(cat $PRGNAM.info | grep "CORE_COMMIT" | cut -d = -f 2 | sed 's/"//g')
KDDW_VER=$(cat $PRGNAM.info | grep "KDDW_VER" | cut -d = -f 2 | sed 's/"//g')
BUILD=${BUILD:-1}
TAG=${TAG:-_idn}
NUMJOBS=${NUMJOBS:-" -j$(expr $(nproc) + 1) "}

# Automatically determine the architecture we're building on:
MARCH=$( uname -m )
if [ -z "$ARCH" ]; then
  case "$MARCH" in
    i?86)    export ARCH=i586 ;;
    armv7hl) export ARCH=$MARCH ;;
    arm*)    export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
    *)       export ARCH=$MARCH ;;
  esac
fi

CWD=$(pwd)
WRK=${WRK:-/tmp/idn/$PRGNAM}
PKG=$WRK/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

# Download source files:
unset DOWNLOAD
unset MD5SUM
eval `cat $PRGNAM.info | grep "DOWNLOAD="`
eval `cat $PRGNAM.info | grep "MD5SUM="`
if [ ! -e $(basename $DOWNLOAD) ]; then
  echo "Downloading: $DOWNLOAD"
  wget -c $DOWNLOAD
fi
while [ $(md5sum $(basename $DOWNLOAD) | cut -d " " -f 1) != $MD5SUM ] ; do
  echo "Downloading: $DOWNLOAD"
  wget -c $DOWNLOAD
done

unset DOWNLOAD1
unset MD5SUM1
eval `cat $PRGNAM.info | grep "DOWNLOAD1="`
eval `cat $PRGNAM.info | grep "MD5SUM1="`
if [ ! -e $(basename $DOWNLOAD1) ]; then
  echo "Downloading: $DOWNLOAD1"
  wget -c $DOWNLOAD1
fi
while [ $(md5sum $(basename $DOWNLOAD1) | cut -d " " -f 1) != $MD5SUM1 ] ; do
  echo "Downloading: $DOWNLOAD1"
  wget -c $DOWNLOAD1
done

unset DOWNLOAD2
unset MD5SUM2
eval `cat $PRGNAM.info | grep "DOWNLOAD2="`
eval `cat $PRGNAM.info | grep "MD5SUM2="`
if [ ! -e $(basename $DOWNLOAD2) ]; then
  echo "Downloading: $DOWNLOAD2"
  wget -c $DOWNLOAD2
fi
while [ $(md5sum $(basename $DOWNLOAD2) | cut -d " " -f 1) != $MD5SUM2 ] ; do
  echo "Downloading: $DOWNLOAD2"
  wget -c $DOWNLOAD2
done

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $WRK $PKG $OUTPUT
cd $WRK
echo "Removing any existing source directory. Please wait..."
rm -rf $SRCNAM-${COMMIT}
tar xvf $CWD/$SRCNAM-${COMMIT}.tar.?z*
cd $SRCNAM-${COMMIT}

cd ext
  rm -rf core KDDockWidgets

  tar xvf $CWD/core-${CORE_COMMIT}.tar.?z*
  mv core-${CORE_COMMIT} core

  tar xvf $CWD/kddockwidgets-${KDDW_VER}.tar.?z*
  mv kddockwidgets-${KDDW_VER} KDDockWidgets
cd -

# Make sure ownerships and permissions are sane:
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# Enable Qt6 VCL support:
if pkg-config --exists Qt6Core ; then
  qt="-DBUILD_QT6=ON"
else
  qt="-DBUILD_QT6=OFF"
fi

# Apply patches:
cat $CWD/patches/opencolorio-2.3.patch | patch -p1 --verbose
cat $CWD/patches/olive-ffmpeg7.patch | patch -p1 --verbose
cat $CWD/patches/core-ffmpeg7.patch | patch -p1 --verbose
cat $CWD/patches/olive-static-helper.patch | patch -p1 --verbose

if pkg-config --exists Qt6Core ; then
  cat $CWD/patches/qt6.patch | patch -p1 --verbose
fi

# Configure, build, and install:
mkdir -p build
cd build
  cmake \
    -GNinja \
    -DCMAKE_CXX_FLAGS="$SLKCFLAGS" \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=lib${LIBDIRSUFFIX} \
    -DOpenGL_GL_PREFERENCE=GLVND \
    -DOTIO_DEPS_INCLUDE_DIR=/usr/include/opentimelineio \
    $qt \
    -DBUILD_SHARED_LIBS=ON \
    -DCMAKE_BUILD_TYPE=Release \
    ..
  ninja $NUMJOBS
  DESTDIR=$PKG ninja install
cd -

# Don't ship .la files:
rm -f $PKG/usr/lib${LIBDIRSUFFIX}/*.la

# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

# Compress info files:
if [ -d $PKG/usr/info ]; then
  ( cd $PKG/usr/info
    rm -f dir
    gzip -9 *
  )
fi

# Compress and if needed symlink the man pages:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

# Add a documentation directory.  Not all of these files are expected to be
# present, but listing them ensures that documentation that might appear and
# disappear from version to version will not be missed.
mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a \
  README.md \
  $PKG/usr/doc/$PRGNAM-$VERSION

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/${PRGNAM}-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tlz}

echo "Cleaning up build directory"
cd $WRK; rm -rf $SRCNAM-${COMMIT} $PKG