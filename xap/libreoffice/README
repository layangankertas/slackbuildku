LibreOffice is a powerful office suite; its clean interface and powerful tools
let you unleash your creativity and grow your productivity. LibreOffice embeds
several applications that make it the most powerful Free & Open Source Office
suite on the market: Writer, the word processor, Calc, the spreadsheet
application, Impress, the presentation engine, Draw, our drawing and
flowcharting application, Base, our database and database frontend,
and Math for editing mathematics.

Build time environment variables that may be set to support additional languages
by overriding the LOLANGS variable, whose default setting is LOLANGS="ar de es
fr id it ja nl vi zh-CN". Note that en-US is always added to whatever LOLANGS is
set. Thus building with, for example, LOLANGS="de" sh LibreOffice.SlackBuild
would build LibreOffice with support for german and US english languages.
Additionally, setting LOLANGS="ALL" will build in support for all available
languages.


This Slackbuild sets GTK3 to be used at runtime. Alternatives (gen, qt5,
kf5, gtk3_kde5) may still be used by setting SAL_USE_VCLPLUGIN in the
user's runtime environment.

Spell checking of documents at runtime requires installation of a suitable
wordlist for the language concerned. This can be achieved in either of two
ways:
1. Build & install hunspell-en (or hunspell-es, hunspell-pl) from SBo
2. Search for the desired language dictionary at:
   http://extensions.libreoffice.org/extension-center?getCategories=Dictionary
and download the relevant file e.g. dict-en.oxt. Now open LO's extension
manager and press the "Add..." button which will open a file browser with which
to locate and open the downloaded .oxt file. The new dictionary will now appear
in the Extension Manager.
