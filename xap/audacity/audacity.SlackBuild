#!/bin/sh

# Slackware build script for audacity

# Copyright 2006-2010 Chess Griffin <chess@chessgriffin.com>
# Copyright 2011-2019 Matteo Bernardini <ponce@slackbuilds.org>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Thanks to Eric Hameleers for adding the audacity.desktop file
# and other tweaks and bug fixes.  Thanks to Heinz Wiesinger for
# bug fixes and the soundtouch and twolame improvements.

# Modified by the SlackBuilds.org project
# Modified 2019 by Ali <idnux09@gmail.com>

PRGNAM=audacity
VERSION=$(cat $PRGNAM.info | grep "VERSION" | cut -d = -f 2 | sed 's/"//g')
BUILD=${BUILD:-1}
TAG=${TAG:-_idn}
NUMJOBS=${NUMJOBS:-" -j$(expr $(nproc) + 1) "}

# Automatically determine the architecture we're building on:
MARCH=$( uname -m )
if [ -z "$ARCH" ]; then
  case "$MARCH" in
    i?86)    export ARCH=i586 ;;
    armv7hl) export ARCH=$MARCH ;;
    arm*)    export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
    *)       export ARCH=$MARCH ;;
  esac
fi

CWD=$(pwd)
WRK=${WRK:-/tmp/idn/$PRGNAM}
PKG=$WRK/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

# Download source files:
unset DOWNLOAD
unset MD5SUM
eval `cat $PRGNAM.info | grep "DOWNLOAD="`
eval `cat $PRGNAM.info | grep "MD5SUM="`
if [ ! -e $(basename $DOWNLOAD) ]; then
  echo "Downloading: $DOWNLOAD"
  wget -c $DOWNLOAD
fi
while [ $(md5sum $(basename $DOWNLOAD) | cut -d " " -f 1) != $MD5SUM ] ; do
  echo "Downloading: $DOWNLOAD"
  wget -c $DOWNLOAD
done

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $WRK $PKG $OUTPUT
cd $WRK
echo "Removing any existing source directory. Please wait..."
rm -rf $PRGNAM-sources-$VERSION
tar xvf $CWD/$PRGNAM-sources-$VERSION.tar.?z*
cd $PRGNAM-sources-$VERSION

# Make sure ownerships and permissions are sane:
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# Enable VST3SDK support:
if pkg-config --exists vst3sdk ; then
  vstdir="/usr/src/vst3sdk"
  vstopt="-Daudacity_use_vst3sdk=system"
else
  vstdir=""
  vstopt="-Daudacity_has_vst3=Off"
fi

# Apply patches:
cat $CWD/patches/audacity-install-rpath.patch | patch -p1 --verbose

# fix vamp plugin search path on x86_64 - thanks to B. Watson
sed -i "s,lib/vamp,lib$LIBDIRSUFFIX/vamp,g" \
  lib-src/libvamp/src/vamp-hostsdk/PluginHostAdapter.cpp

# libsoxr is the new default resampling library

#Included in src/AboutDialog.cpp but not supplied
touch include/RevisionIdent.h

# Configure, build, and install:
mkdir -p build
cd build
  VST3SDK=$vstdir \
  cmake \
    -GNinja \
    -DCMAKE_C_FLAGS="$SLKCFLAGS -DNDEBUG" \
    -DCMAKE_CXX_FLAGS="$SLKCFLAGS -DHAVE_VISIBILITY=1 -DNDEBUG" \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=/usr/lib${LIBDIRSUFFIX} \
    -DCMAKE_INSTALL_MANDIR=/usr/man \
    -DCMAKE_CXX_STANDARD=17 \
    -DAUDACITY_BUILD_LEVEL=2 \
    -Daudacity_conan_enabled=Off \
    -Daudacity_lib_preference=system \
    -Daudacity_use_ffmpeg=loaded \
    -Daudacity_use_expat=system \
    -Daudacity_use_lame=system \
    -Daudacity_use_ladspa=On \
    -Daudacity_use_lv2=system \
    -Daudacity_use_midi=system \
    -Daudacity_use_portaudio=system \
    -Daudacity_use_soundtouch=system \
    -Daudacity_use_twolame=system \
    -Daudacity_use_vamp=system \
    -Daudacity_use_wxwidgets=system \
    -Daudacity_use_pch=Off \
    $vstopt \
    -Daudacity_has_networking=Off \
    -Daudacity_has_crashreports=Off \
    -Daudacity_has_updates_check=Off \
    -Daudacity_has_sentry_reporting=Off \
    -Wno-dev \
    -DCMAKE_BUILD_TYPE=Release \
    ..
  ninja $NUMJOBS
  DESTDIR=$PKG ninja install
cd -

# Don't ship .la files:
rm -f $PKG/{,usr/}lib${LIBDIRSUFFIX}/*.la

# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

# Compress info files:
if [ -d $PKG/usr/info ]; then
  ( cd $PKG/usr/info
    rm -f dir
    gzip -9 *
  )
fi

# Compress and if needed symlink the man pages:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

# Add a documentation directory.  Not all of these files are expected to be
# present, but listing them ensures that documentation that might appear and
# disappear from version to version will not be missed.
mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
rm -fr $PKG/usr/share/doc
cp -a \
  CHANGELOG.txt INSTALL LICENSE.txt README.md \
  $PKG/usr/doc/$PRGNAM-$VERSION

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/${PRGNAM}-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tlz}

echo "Cleaning up build directory"
cd $WRK; rm -rf $PRGNAM-sources-$VERSION $PKG