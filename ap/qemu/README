QEMU is a generic and open source machine emulator and virtualizer.

When used as a machine emulator, QEMU can run OSes and programs made for
one machine (e.g. an ARM board) on a different machine (e.g. your PC).
By using dynamic translation, it achieves very good performances.

qemu (with kvm enabled) achieves near native performances by leveraging
the kvm-kmod modules and executing the guest code directly on the host
CPU.  Slackware provides pre-built 32/64 bit x86 kvm-kmod modules or you
can build different versions with the kvm-kmod package.

Don't forget to load the 'kvm-intel' or 'kvm-amd' module (depending on
your processor) prior to launching qemu-system-ARCH with kvm enabled.
For older/unmaintained qemu frontends, this build also creates a symlink
to qemu-system-ARCH at /usr/bin/qemu-kvm.

NOTES:
  This version breaks some backward compatibility with earlier versions.
  Consult the official changelogs for details.

  SDL 1.2 support is deprecated, SDL 2.0 should be used instead.
  Python 3.x can be used to build QEMU.
