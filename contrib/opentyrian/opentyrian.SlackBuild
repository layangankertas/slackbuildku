#!/bin/sh

# Slackware build script for opentyrian

# Written by B. Watson (yalhcru@gmail.com)

# Licensed under the WTFPL. See http://www.wtfpl.net/txt/copying/ for details.

# 20170620 bkw: fix compile on -current (still works fine on 14.2)
# Modified 2019 by Ali <idnux09@gmail.com>

PRGNAM=opentyrian
VERSION=$(cat $PRGNAM.info | grep "VERSION" | cut -d = -f 2 | sed 's/"//g')
DATAVER=${DATAVER:-21}
BUILD=${BUILD:-4}
TAG=${TAG:-_idn}
NUMJOBS=${NUMJOBS:-" -j$(expr $(nproc) + 1) "}

# Automatically determine the architecture we're building on:
MARCH=$( uname -m )
if [ -z "$ARCH" ]; then
  case "$MARCH" in
    i?86)    export ARCH=i586 ;;
    armv7hl) export ARCH=$MARCH ;;
    arm*)    export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
    *)       export ARCH=$MARCH ;;
  esac
fi

CWD=$(pwd)
WRK=${WRK:-/tmp/idn/$PRGNAM}
PKG=$WRK/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

# Download source files:
unset DOWNLOAD
unset MD5SUM
eval `cat $PRGNAM.info | grep "DOWNLOAD="`
eval `cat $PRGNAM.info | grep "MD5SUM="`
if [ ! -e $(basename $DOWNLOAD) ]; then
  echo "Downloading: $DOWNLOAD"
  wget -c $DOWNLOAD
fi
while [ $(md5sum $(basename $DOWNLOAD) | cut -d " " -f 1) != $MD5SUM ] ; do
  echo "Downloading: $DOWNLOAD"
  wget -c $DOWNLOAD
done

unset DOWNLOAD1
unset MD5SUM1
eval `cat $PRGNAM.info | grep "DOWNLOAD1="`
eval `cat $PRGNAM.info | grep "MD5SUM1="`
if [ ! -e $(basename $DOWNLOAD1) ]; then
  echo "Downloading: $DOWNLOAD1"
  wget -c $DOWNLOAD1
fi
while [ $(md5sum $(basename $DOWNLOAD1) | cut -d " " -f 1) != $MD5SUM1 ] ; do
  echo "Downloading: $DOWNLOAD1"
  wget -c $DOWNLOAD1
done

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $WRK $PKG $OUTPUT
cd $WRK
echo "Removing any existing source directory. Please wait..."
rm -rf $PRGNAM-$VERSION
tar xvf $CWD/$PRGNAM-$VERSION.tar.?z*
cd $PRGNAM-$VERSION

# Make sure ownerships and permissions are sane:
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# Apply patches:
cat $CWD/patches/2.1.20130907-cflag-idiocy.diff | patch -p1 --verbose
cat $CWD/patches/data_dir.patch | patch -p1 --verbose
cat $CWD/patches/2.1.20130907-gcc10.patch | patch -p1 --verbose

# Build and install:
mkdir -p \
  $PKG/usr/games \
  $PKG/usr/share/$PRGNAM/data \
  $PKG/usr/man/man6 \
  $PKG/usr/share/pixmaps \
  $PKG/usr/share/applications \
  $PKG/usr/doc/$PRGNAM-$VERSION \
  $PKG/install

sed -i "s,-g0,$SLKCFLAGS," Makefile
sed -i "s,-g3.*\$,$SLKCFLAGS," Makefile
make $NUMJOBS all

install -s $PRGNAM $PKG/usr/games

# Probably some of the data files are redundant. We certainly don't
# need the DOS executables or their docs (except manual.doc which
# has the game story).
cat <<EOF > $PKG/usr/doc/$PRGNAM-$VERSION/manual.txt
20140429 bkw: This is the original manual from the DOS game. The install
instructions are obsolete, but the backstory and gameplay instructions
are still relevant [beginning with V) THE STORY].

EOF

unzip $CWD/tyrian$DATAVER.zip
cd tyrian$DATAVER
  chmod 644 *
  sed 's/\r//' manual.doc >> $PKG/usr/doc/$PRGNAM-$VERSION/manual.txt
  rm -f *.exe *.doc
  mv * $PKG/usr/share/$PRGNAM/data
cd -

cp linux/man/$PRGNAM.6 $PKG/usr/man/man6
cat linux/icons/tyrian-128.png > $PKG/usr/share/pixmaps/$PRGNAM.png
cat linux/$PRGNAM.desktop > $PKG/usr/share/applications/$PRGNAM.desktop

# Don't ship .la files:
rm -f $PKG/{,usr/}lib${LIBDIRSUFFIX}/*.la

# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

# Compress info files:
if [ -d $PKG/usr/info ]; then
  ( cd $PKG/usr/info
    rm -f dir
    gzip -9 *
  )
fi

# Compress and if needed symlink the man pages:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

# Add a documentation directory.  Not all of these files are expected to be
# present, but listing them ensures that documentation that might appear and
# disappear from version to version will not be missed.
chmod 644 CREDITS
cp -a \
  COPYING CREDITS NEWS README \
  $PKG/usr/doc/$PRGNAM-$VERSION

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/${PRGNAM}-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tlz}

echo "Cleaning up build directory"
cd $WRK; rm -rf $PRGNAM-$VERSION $PKG