#!/bin/sh

# Slackware build script for ffmpeg

# Copyright 2010-2017 Heinz Wiesinger, Amsterdam, The Netherlands
# Copyright 2017  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Originally written by core (eroc@linuxmail.org)
# Modified by Robby Workman <rworkman@slackbuilds.org>
# Modified 2019 by Ali <idnux09@gmail.com>

PRGNAM=ffmpeg
VERSION=$(cat $PRGNAM.info | grep "VERSION" | cut -d = -f 2 | sed 's/"//g')
BUILD=${BUILD:-1}
TAG=${TAG:-_idn}
NUMJOBS=${NUMJOBS:-" -j$(expr $(nproc) + 1) "}

# Automatically determine the architecture we're building on:
MARCH=$( uname -m )
if [ -z "$ARCH" ]; then
  case "$MARCH" in
    i?86)    export ARCH=i586 ;;
    armv7hl) export ARCH=$MARCH ;;
    arm*)    export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
    *)       export ARCH=$MARCH ;;
  esac
fi

CWD=$(pwd)
WRK=${WRK:-/tmp/idn/$PRGNAM}
PKG=$WRK/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

# Download source files:
unset DOWNLOAD
unset MD5SUM
eval `cat $PRGNAM.info | grep "DOWNLOAD="`
eval `cat $PRGNAM.info | grep "MD5SUM="`
if [ ! -e $(basename $DOWNLOAD) ]; then
  echo "Downloading: $DOWNLOAD"
  wget -c $DOWNLOAD
fi
while [ $(md5sum $(basename $DOWNLOAD) | cut -d " " -f 1) != $MD5SUM ] ; do
  echo "Downloading: $DOWNLOAD"
  wget -c $DOWNLOAD
done

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $WRK $PKG $OUTPUT
cd $WRK
echo "Removing any existing source directory. Please wait..."
rm -rf $PRGNAM-$VERSION
tar xvf $CWD/$PRGNAM-$VERSION.tar.?z*
cd $PRGNAM-$VERSION

# Make sure ownerships and permissions are sane:
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# Enable AMD Advanced Media Framework support for AMD GPU:
if test -d /usr/include/AMF ; then
  amd="--enable-amf"
else
  amd="--disable-amf"
fi

# Enable Intel Quick Sync Video support for Intel GPU:
if pkg-config --exists vpl ; then
  intel="--enable-libvpl --disable-libmfx"
elif pkg-config --exists libmfx ; then
  intel="--enable-libmfx --disable-libvpl"
else
  intel="--disable-libmfx"
fi

# Enable NVCODEC support for Nvidia GPU:
if pkg-config --exists ffnvcodec ; then
  nvidia="--enable-ffnvcodec --enable-nvdec --enable-nvenc --enable-vdpau"
else
  nvidia="--disable-ffnvcodec --disable-nvdec --disable-nvenc --disable-vdpau"
fi

# Enable CUDA support for Nvidia GPU:
if pkg-config --exists cuda ; then
  cuda="--enable-cuda-nvcc --enable-cuvid"
else
  cuda="--disable-cuda-nvcc --disable-cuvid"
fi

# Apply patches:
cat $CWD/patches/add-av_stream_get_first_dts-for-chromium.patch | patch -p1 --verbose
cat $CWD/patches/Add-ability-to-run-svt-vp9.patch | patch -p1 --verbose
cat $CWD/patches/add-external-dec-libvvdec-for-H266-VVC.patch | patch -p1 --verbose

# Allow to build with fdk-aac-free
cat $CWD/patches/allow-fdk-aac-free.patch | patch -p1 --verbose

# Apply some patches for Intel GPU:
if pkg-config --exists vpl || pkg-config --exists libmfx ; then
  cat $CWD/patches/intel/0021-lavc-decode-Add-get_hw_config-function.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0022-lavc-decode-Add-internal-surface-re-allocate-method-.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0023-lavc-vaapi_vp9-add-surface-internal-re-allocation-ca.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0033-lavc-vaapi_hevc-add-skip_frame-invalid-to-skip-inval.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0034-FFmpeg-vaapi-HEVC-SCC-encode.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0050-libavcodec-qsvdec.c-extract-frame-packing-arrangemen.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0055-libavcodec-qsvenc_hevc-add-main10sp-support-to-hevc_.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0060-avutils-hwcontext-When-deriving-a-hwdevice-search-fo.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0062-lavu-add-sub-frame-side-data.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0063-lavc-add-sub-frame-options-and-flag.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0064-lavc-hevc_vaapi-enable-sub-frame-support.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0065-examples-seperate-vaapi_decode-from-hw_decode.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0066-lavfi-Add-exportsubframe-filter.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0067-avfilter-vf_xcam-add-xcam-video-filter.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0075-lavf-dnn-enable-libtorch-backend-support-FRVSR-model.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0092-avutil-hwcontext-add-a-function-to-get-the-AVHWDevic.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0093-avfilter-vf_hwmap-get-the-AVHWDeviceType-from-outlin.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0094-lavfi-avfiltergraph-move-convert-codes-into-function.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0095-lavfi-format-wrap-auto-filters-into-structures.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0096-lavfi-format-add-a-hwmap-auto-conversion-filter.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0116-lavfi-add-LibTorch-backend-multi-frame-inference-sup.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0117-libavfilter-dnn-dnn_backend_torch-Add-async-to-torch.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0118-libavfilter-dnn-dnn_backend_torch-add-multi-device-s.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0132-ffmpeg-raisr-filter.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0133-libavfilter-raisr_opencl-Add-raisr_opencl-filter.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0135-avutil-hwcontext_d3d11va-enable-D3D11_RESOURCE_MISC_.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0151-libavfilter-dnn_backend_openvino-Factor-out-preproce.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0152-libavfilter-dnn_backend_openvino-Add-remote_context-.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0153-libavfilter-vf_dnn_detect-Add-vaapi-into-detect.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0154-libavfilter-dnn_backend_openvino-Add-vaSurface-crop-.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0155-libavfilter-vf_dnn_classify-Add-vaapi-into-classify.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0157-lavc-vaapi_dec-Create-VA-parameters-dynamically.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0158-lavc-vaapi_decode-Use-a-more-meaningful-variable-nam.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0159-lavc-cbs_h266-Add-SliceTopLeftTileIdx-to-H266RawPPS.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0160-lavc-cbs_h266-Add-NumSlicesInTile-to-H266RawPPS.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0161-lavc-vvc_refs-Define-FF_VVC_FRAME_FLAG-to-h-header.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0162-lavc-vvc_ps-Add-alf-raw-syntax-into-VVCALF.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0163-lavc-vvc_dec-Add-hardware-decode-API.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0164-lavc-vaapi_dec-Add-VVC-decoder.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0165-libavcodec-qsvenc-enable-Alpha-Encode-for-HEVC.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0166-libavcodec-qsvenc-enable-Screen-Content-Tool-Encode-.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0167-lavc-qsvenc-Support-calculate-encoded-frame-quality-.patch | patch -p1 --verbose
  cat $CWD/patches/intel/0168-lavc-qsvenc-Make-ffmpeg-compilable-with-experimental.patch | patch -p1 --verbose
fi

# Configure, build, and install:
CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --shlibdir=/usr/lib${LIBDIRSUFFIX} \
  --mandir=/usr/man \
  --docdir=/usr/doc/$PRGNAM-$VERSION/html \
  --arch=$ARCH \
  --enable-gpl \
  --enable-version3 \
  --enable-ffplay \
  --enable-ffprobe \
  --enable-frei0r \
  --enable-ladspa \
  --enable-libaom \
  --enable-libass \
  --enable-libbs2b \
  --enable-libcaca \
  --enable-libcdio \
  --enable-libdav1d \
  --enable-libdrm \
  --enable-libdvdnav \
  --enable-libdvdread \
  --enable-libfdk-aac \
  --enable-libfontconfig \
  --enable-libfreetype \
  --enable-libfribidi \
  --enable-libgsm \
  --enable-libjack \
  --enable-libjxl \
  --enable-libmp3lame \
  --enable-libmysofa \
  --enable-libopencore-amrnb \
  --enable-libopencore-amrwb \
  --enable-libopenh264 \
  --enable-libopenjpeg \
  --enable-libopus \
  --enable-libpulse \
  --enable-librav1e \
  --enable-librist \
  --enable-librsvg \
  --enable-librubberband \
  --enable-libshaderc \
  --enable-libsmbclient \
  --enable-libspeex \
  --enable-libsrt \
  --enable-libssh \
  --enable-libsvtav1 \
  --enable-libsvtvp9 \
  --enable-libtheora \
  --enable-libtwolame \
  --enable-libv4l2 \
  --enable-libvidstab \
  --enable-libvorbis \
  --enable-libvpx \
  --enable-libvvdec \
  --enable-libvvenc \
  --enable-libwebp \
  --enable-libx264 \
  --enable-libx265 \
  --enable-libxml2 \
  --enable-libxvid \
  --enable-libzimg \
  --enable-lto \
  --enable-lv2 \
  --enable-alsa \
  --enable-libdrm \
  --enable-openal \
  --enable-opengl \
  --enable-opencl \
  --enable-sdl2 \
  --enable-vaapi \
  --enable-vulkan \
  $amd \
  $intel \
  $nvidia \
  $cuda \
  --disable-debug \
  --enable-shared \
  --disable-static

make $NUMJOBS
make install DESTDIR=$PKG
make install-man DESTDIR=$PKG

# Don't ship .la files:
rm -f $PKG/{,usr/}lib${LIBDIRSUFFIX}/*.la

# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

# Compress info files:
if [ -d $PKG/usr/info ]; then
  ( cd $PKG/usr/info
    rm -f dir
    gzip -9 *
  )
fi

# Compress and if needed symlink the man pages:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

# Add a documentation directory.  Not all of these files are expected to be
# present, but listing them ensures that documentation that might appear and
# disappear from version to version will not be missed.
mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION/txt
cp -a \
  COPYING* CREDITS INSTALL* LICENSE* MAINTAINERS README* \
  RELEASE VERSION \
  $PKG/usr/doc/$PRGNAM-$VERSION
cp -a doc/*.txt $PKG/usr/doc/$PRGNAM-$VERSION/txt/
find . -name "RELEASE_NOTES" -exec cp -a {} $PKG/usr/doc/$PRGNAM-$VERSION/ \;

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/${PRGNAM}-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tlz}

echo "Cleaning up build directory"
cd $WRK; rm -rf $PRGNAM-$VERSION $PKG