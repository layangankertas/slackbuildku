config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}

config etc/dcmtk-@VERSION@/dcmpstat.cfg.new
config etc/dcmtk-@VERSION@/dcmqrscp.cfg.new
config etc/dcmtk-@VERSION@/filelog.cfg.new
config etc/dcmtk-@VERSION@/logger.cfg.new
config etc/dcmtk-@VERSION@/printers.cfg.new
config etc/dcmtk-@VERSION@/storescp.cfg.new
config etc/dcmtk-@VERSION@/storescu.cfg.new
