#!/bin/sh

# Slackware build script for wxWidgets

# Copyright 2013-2018 Willy Sudiarto Raharjo <willysr@slackbuilds.org>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# Modified 2019 by Ali <idnux09@gmail.com>

PRGNAM=wxWidgets
VERSION=$(cat $PRGNAM.info | grep "VERSION" | cut -d = -f 2 | sed 's/"//g')
BUILD=${BUILD:-1}
TAG=${TAG:-_idn}
NUMJOBS=${NUMJOBS:-" -j$(expr $(nproc) + 1) "}
SHORTVER=${VERSION%.*}

# Automatically determine the architecture we're building on:
MARCH=$( uname -m )
if [ -z "$ARCH" ]; then
  case "$MARCH" in
    i?86)    export ARCH=i586 ;;
    armv7hl) export ARCH=$MARCH ;;
    arm*)    export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
    *)       export ARCH=$MARCH ;;
  esac
fi

CWD=$(pwd)
WRK=${WRK:-/tmp/idn/$PRGNAM}
PKG=$WRK/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

# Download source files:
unset DOWNLOAD
unset MD5SUM
eval `cat $PRGNAM.info | grep "DOWNLOAD="`
eval `cat $PRGNAM.info | grep "MD5SUM="`
if [ ! -e $(basename $DOWNLOAD) ]; then
  echo "Downloading: $DOWNLOAD"
  wget -c $DOWNLOAD
fi
while [ $(md5sum $(basename $DOWNLOAD) | cut -d " " -f 1) != $MD5SUM ] ; do
  echo "Downloading: $DOWNLOAD"
  wget -c $DOWNLOAD
done

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $WRK $PKG $OUTPUT
cd $WRK
echo "Removing any existing source directory. Please wait..."
rm -rf $PRGNAM-$VERSION
mkdir -p $PRGNAM-$VERSION
cd $PRGNAM-$VERSION
  tar xvf $CWD/$PRGNAM-$VERSION.tar.?z*

# Make sure ownerships and permissions are sane:
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# Apply patches:
cd $PRGNAM-$VERSION
  cat $CWD/patches/make-abicheck-non-fatal.patch | patch -p1 --verbose
  cat $CWD/patches/fix-linking.patch | patch -p1 --verbose
  cat $CWD/patches/fix-qt.patch | patch -p1 --verbose
cd -

# Configure, build, and install:
cp -r $PRGNAM-$VERSION build-gtk3
cp -r $PRGNAM-$VERSION build-qt5

cd build-qt5
  CFLAGS="$SLKCFLAGS -fno-strict-aliasing" \
  CXXFLAGS="$SLKCFLAGS -fno-strict-aliasing" \
  LDFLAGS="-Wl,--as-needed" \
  ./configure \
    --prefix=/usr \
    --libdir=/usr/lib${LIBDIRSUFFIX} \
    --sysconfdir=/etc \
    --infodir=/usr/info \
    --mandir=/usr/man \
    --docdir=/usr/doc/$PRGNAM-$VERSION \
    --with-qt \
    --with-opengl \
    --with-sdl \
    --with-regex=builtin \
    --with-themes=all \
    --with-libpng=sys \
    --with-libxpm=sys \
    --with-libjpeg=sys \
    --with-libtiff=sys \
    --with-zlib=sys \
    --with-liblzma=yes \
    --with-libnotify=sys \
    --with-expat=sys \
    --with-gtkprint \
    --with-subdirs \
    --enable-precomp-headers \
    --enable-all-features \
    --enable-plugins \
    --enable-controls \
    --enable-compat28 \
    --enable-compat30 \
    --enable-mediactrl \
    --enable-sound \
    --enable-stc \
    --enable-gui \
    --enable-graphics_ctx \
    --enable-display \
    --enable-geometry \
    --enable-unicode \
    --enable-intl \
    --enable-ipv6 \
    --enable-webview \
    --enable-privatefonts \
    --enable-option-checking \
    --enable-uiactionsim \
    --enable-autoidman \
    --enable-zipstream \
    --enable-splash \
    --enable-textdlg \
    --enable-permissive \
    --enable-std_containers_compat \
    --enable-optimise \
    --disable-rpath \
    --enable-shared \
    --build=$ARCH-slackware-linux

  make $NUMJOBS
  make install DESTDIR=$PKG

  ln -s /usr/lib${LIBDIRSUFFIX}/wx/config/qt-unicode-$SHORTVER $PKG/usr/bin/wx-config-qt5
cd -

cd build-gtk3
  CFLAGS="$SLKCFLAGS -fno-strict-aliasing" \
  CXXFLAGS="$SLKCFLAGS -fno-strict-aliasing" \
  LDFLAGS="-Wl,--as-needed" \
  ./configure \
    --prefix=/usr \
    --libdir=/usr/lib${LIBDIRSUFFIX} \
    --sysconfdir=/etc \
    --infodir=/usr/info \
    --mandir=/usr/man \
    --docdir=/usr/doc/$PRGNAM-$VERSION \
    --with-gtk=3 \
    --with-opengl \
    --with-sdl \
    --with-regex=builtin \
    --with-themes=all \
    --with-libpng=sys \
    --with-libxpm=sys \
    --with-libjpeg=sys \
    --with-libtiff=sys \
    --with-zlib=sys \
    --with-liblzma=yes \
    --with-libnotify=sys \
    --with-expat=sys \
    --with-gtkprint \
    --with-subdirs \
    --enable-precomp-headers \
    --enable-all-features \
    --enable-plugins \
    --enable-controls \
    --enable-compat28 \
    --enable-compat30 \
    --enable-mediactrl \
    --enable-sound \
    --enable-stc \
    --enable-gui \
    --enable-graphics_ctx \
    --enable-display \
    --enable-geometry \
    --enable-unicode \
    --enable-intl \
    --enable-ipv6 \
    --enable-webview \
    --enable-privatefonts \
    --enable-option-checking \
    --enable-uiactionsim \
    --enable-autoidman \
    --enable-zipstream \
    --enable-splash \
    --enable-textdlg \
    --enable-permissive \
    --enable-std_containers_compat \
    --enable-optimise \
    --disable-rpath \
    --enable-shared \
    --build=$ARCH-slackware-linux

  make $NUMJOBS
  make install DESTDIR=$PKG
  make $NUMJOBS -C locale allmo

  ln -s /usr/lib${LIBDIRSUFFIX}/wx/config/gtk3-unicode-$SHORTVER $PKG/usr/bin/wx-config-gtk3
cd -

cd $PRGNAM-$VERSION

# Don't ship .la files:
rm -f $PKG/usr/lib${LIBDIRSUFFIX}/*.la

# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

# Compress info files:
if [ -d $PKG/usr/info ]; then
  ( cd $PKG/usr/info
    rm -f dir
    gzip -9 *
  )
fi

# Compress and if needed symlink the man pages:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

# Add a documentation directory.  Not all of these files are expected to be
# present, but listing them ensures that documentation that might appear and
# disappear from version to version will not be missed.
mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a \
  docs/* \
  $PKG/usr/doc/$PRGNAM-$VERSION

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/${PRGNAM}-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tlz}

echo "Cleaning up build directory"
cd $WRK; rm -rf $PRGNAM-$VERSION $PKG